import "./App.css";
import vamklogo from "./vamk.png";

function App() {
  return (
    <div className="card-wrapper">
      <div className="card">
        <img src={vamklogo} alt="picture" />
        <span>Vaasan ammattikorkeakoulu</span>
        <span>University of applied sciences</span>
        <br />
        <span style={{ fontWeight: "bold", color: "violet" }}>Toni Tuomaala</span>
        <span>Tietotekniikka</span>
        <span>Opiskelija</span>
        <br />
        <span>0505010966</span>
        <span>e2101140@edu.vamk.fi</span>
        <br />
        <span>Wolffintie 30, FI-65200, Vaasa, Finland</span>
      </div>
    </div>
  );
}

export default App;